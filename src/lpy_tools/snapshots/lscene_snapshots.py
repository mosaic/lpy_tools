#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Ayan Chaudhury <ayan.chaudhury@inria.fr>
#
#       File contributor(s):
#           Ayan Chaudhury <ayan.chaudhury@inria.fr>
#           Christophe Godin <christophe.godin@inria.fr>
#
#       File maintainer(s):
#           Christophe Godin <christophe.godin@inria.fr
#
# -----------------------------------------------------------------------

"""
Library of tools to take snapshots of the result of an L-System simulations.
"""

import os
from math import pi

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
from numpy import arange
from openalea import lpy
from openalea.plantgl.algo import ZBufferEngine
from openalea.plantgl.algo import eColorBased
from openalea.plantgl.math import Matrix3
from openalea.plantgl.math import Vector3
from openalea.plantgl.math import norm
from openalea.plantgl.scenegraph import BoundingBox
from openalea.plantgl.scenegraph import Color3


def read_camera(filename):
    """ Camera file contains
        e.g.:
        104 10 4 -0.583715 -0.430251 35.4873 149.352 -0.430251 35.4873 92 -2.2 -1.8 40.0189 480.226 0.600283 30 79.7913 1 1
        interpreted as:
        azimuth, Elevation, ?4, Center(3-uple), Eye(3-uple), Zoom, Translation(2-uple),?40.0189, zfar, znear, DefaultViewAngle(deg), CurrentViewAngle(deg), ?1, ?1
    """
    with open(filename, 'r') as file:
        data = file.read().replace('\n', '')
    tokens = data.split()

    camdict = {
        'azimuth': float(tokens[0]),  # Azimuth of the camera frame (originally positioned at eye position)
        'elevation': float(tokens[1]),  # Elevation of camera frame (originally positioned at eye position)
        'stepmove': int(tokens[2]),  # ReadOnly - cannot be changed by API
        'center': (float(tokens[3]), float(tokens[4]), float(tokens[5])),
        'eye': (float(tokens[6]), float(tokens[7]), float(tokens[8])),
        # camera position de reference, direction is by definition towards -x
        'zoomcoef': float(tokens[9]),
        'Translation': (float(tokens[10]), float(tokens[11])),  # to translate the camera after azimuth and elevation
        'radius': float(tokens[12]),  # radius of the scene
        'zfar': float(tokens[13]),
        # defines the plane on which the scene is projected as the distance between camera and object that should not intersect visualized objects
        'znear': float(tokens[14]),
        'DefaultViewAngle': (float(tokens[15])),
        'CurrentViewAngle': (float(tokens[16])),
        'projection_mode': int(tokens[17]),  # Flag orthographic, pers
        'geomsys': int(tokens[18]),  # Flag PlanGL frame (Z up) / OpenGL (Y up)
    }
    """
    target_point = None             # looks a bit above z = 0. Value is a 3-uple (x,y,z)
    zoomcoef = 1.                   # increase to zoom out
    camdist = 1.                    # increase to widen the observation window
    elevation = 30                  # elevation of the camera sight (in degrees)
    azimuth = 0.                    # Azimuth of the camera
    bb = None                       # Bounding box
    width = 400                     # Window size in pixels
    height = 400                    # Window size in pixels
    aspectratio = 1.0               # contraction of X and Y direction defined by Height/Width
    znear = 0.1                     # defines the plane on which the scene is projected as the distance between camera and object that should not intersect visualized objects
    zfar = 200                      # Same thing for the farthest plane in the camera. Only objects between these two planes can be seen
    lightpos = (0, 0, 10)           # light characteristics 
    ambiant = (255, 255, 255)
    diffuse = (0, 0, 0)
    specular = (0, 0, 0))
    """

    return camdict


def get_bb(viewer):
    """
    gets the bounding box of a scene contained in a LPy viewer.
    """
    sc = viewer.getCurrentScene()
    return BoundingBox(sc)


# No longer used (prefer offline equivalent function below)
def take_snapshot(viewer, filename_prefix=".", suffix="",
                  cam_settings={'camdist': 1., 'zoomcoef': 1., 'bb': None, 'target_point': None}):
    """
    take a snapshot of a computed lpy_scene, with camera attributes defined in cam_settings:
    - target_point is the point at which the camera is looking (Vector3)
    - camdist fixes the distance of the camera to the target point (in screen units)
    - zoomcoef is a multiplicative zoom factor (higher means zooming out)
    - azimuth is an angle in degrees
    - elevation is a height in screen units with respect to the (x,y) plane (applied after having zoomed in/out)

    filename_prefix: A prefix directory may be defined (default is current directory '.')
    suffix: name of the image
    """

    if 'camdist' in cam_settings:
        camdist = cam_settings['camdist']
    else:
        camdist = None
    if 'bb' in cam_settings:
        bb = cam_settings['bb']
    else:
        bb = None
    if 'zoomcoef' in cam_settings:
        zoomcoef = cam_settings['zoomcoef']
    else:
        zoomcoef = 1.0
    if 'target_point' in cam_settings:
        target_point = cam_settings['target_point']
    else:
        target_point = None
    if 'elevation' in cam_settings:
        elevation = cam_settings['elevation']
    else:
        elevation = 0.0  # elevation in screen units along the z-axis
    if 'azimuth' in cam_settings:
        azimuth = cam_settings['azimuth']
    else:
        azimuth = 0.0

    # Determine the point to look at
    if target_point == None:
        # define bounding box used to take the picture
        if bb == None:
            # computes the bounding box of this scene
            bbx = get_bb(viewer)
        else:
            # computes the bounding box of this scene
            bbx = bb
        c = bbx.getCenter()
    else:
        c = target_point  # target point to look at is given as an argument

    # computes the  distance of the camera to the target point.
    if camdist == None:
        # p = position of the camera
        # h = head (shooting direction)
        # u = up (upward direction of the shooting)
        p, h, u = viewer.camera.getPosition()

        dist = norm(p - c)  # * camdist_factor
        # print ("scene center = ", c)
        # print ("camera position = ", p,h,u)
        # print ("camera distance = ", camdist)
    else:
        dist = camdist

    # moves the position of the camera from (1,0,0) with origin in (0,0,0)
    # to (r,theta,z)=(dist*zoomceof,azimuth, elevation) with origin in c
    np = c + Matrix3.axisRotation((0, 0, 1), azimuth * pi / 180) * Vector3(1, 0, 0) * dist * zoomcoef
    np = np + Vector3(0, 0, elevation)

    # Defines the view configuration
    # To dispay or not the grids
    viewer.grids.set(False, False, False, False)
    # Set beckground Color
    viewer.frameGL.setBgColor(Color3(255, 255, 255))
    # Sets the size of the PlantGL window in pixels
    viewer.frameGL.setSize(400, 400)

    # Defines the new camera position and orientation as
    # shooting direction = c-np
    # up vector of the camera keeps oriented towards Z
    viewer.camera.lookAt(np, c)
    iname = filename_prefix + '/' + str(suffix) + '.png'
    # print("2:image name = ", iname)
    viewer.saveSnapshot(iname)
    return iname


def take_offline_snapshot(lscene, filename_prefix=".", suffix="",
                          cam_settings={'camdist': 1., 'zoomcoef': 1., 'bb': None, 'target_point': None}):
    """Take a snapshot of a computed lpy_scene without the need of running L-Py.

    Parameters
    ----------
    lscene : ???
        ???
    filename_prefix: str, optional
        A prefix directory may be defined (default is current directory '.')
    suffix : str, optional
        Name of the image.
    cam_settings : dict
        A dictionary of camera settings. See ``grid_simulate_and_shoot``.

    Returns
    -------
    str
        Name of the png file.

    Notes
    -----
    Camera attributes defined in `cam_settings`:
    - target_point is the point at which the camera is looking (Vector3)
    - camdist fixes the distance of the camera to the target point (in screen units)
    - zoomcoef is a multiplicative zoom factor (higher means zooming out)
    - azimuth is an angle in degrees
    - elevation is a height in screen units with respect to the (x,y) plane (applied after having zoomed in/out)

    """

    #  May be try this to simplyfy what follows
    #  d = {'a': 1, 'b': 2}
    #  for key, val in d.items():
    #    exec(key + '=val')

    if 'camdist' in cam_settings:
        camdist = cam_settings['camdist']
    else:
        camdist = 1.
    if 'bb' in cam_settings:
        bb = cam_settings['bb']
    else:
        bb = None
    if 'zoomcoef' in cam_settings:
        zoomcoef = cam_settings['zoomcoef']
    else:
        zoomcoef = 1.0
    if 'target_point' in cam_settings:
        target_point = cam_settings['target_point']
    else:
        target_point = None
    if 'elevation' in cam_settings:
        elevation = cam_settings['elevation']
    else:
        elevation = 0.0  # elevation in screen units along the z-axis
    if 'azimuth' in cam_settings:
        azimuth = cam_settings['azimuth']
    else:
        azimuth = 0.0
    if 'width' in cam_settings:
        width = cam_settings['width']
    else:
        width = 400
    if 'height' in cam_settings:
        height = cam_settings['height']
    else:
        height = 400
    if 'aspectratio' in cam_settings:
        aspectratio = cam_settings['aspectratio']
    else:
        aspectratio = 1.
    if 'znear' in cam_settings:
        znear = cam_settings['znear']
    else:
        znear = 0.1
    if 'zfar' in cam_settings:
        zfar = cam_settings['zfar']
    else:
        zfar = 200
    if 'lightpos' in cam_settings:
        lightpos = cam_settings['lightpos']
    else:
        lightpos = (0, 0, 10)
    if 'ambiant' in cam_settings:
        ambiant = cam_settings['ambiant']
    else:
        ambiant = (255, 255, 255)
    if 'diffuse' in cam_settings:
        diffuse = cam_settings['diffuse']
    else:
        diffuse = (0, 0, 0)
    if 'specular' in cam_settings:
        specular = cam_settings['specular']
    else:
        specular = (0, 0, 0)

    # Determine the point to look at
    if target_point == None:
        # define bounding box used to take the picture
        if bb == None:
            # computes the bounding box of this scene
            bbx = BoundingBox(lscene)
        else:
            # computes the bounding box of this scene
            bbx = bb
        c = bbx.getCenter()
    else:
        c = target_point  # target point to look at is given as an argument

    # DISPLAY WINDOW

    # rendering engine (alternative offline to the viewer
    zb = ZBufferEngine(width, height, (255, 255, 255), renderingStyle=eColorBased)
    zb.multithreaded = False

    # PERSPECTIVE SETTINGS

    # Clipping planes:
    # near plane = plane ortho to the camera sight defined by its distance to the camera: znear
    # far plane  = plane ortho to the camera sight defined by its distance to the camera: znear

    #
    vertical_view_angle = 30  # in degrees
    zb.setPerspectiveCamera(vertical_view_angle, aspectratio, znear, zfar)

    # CAMERA SIGHT
    # np = camera position
    # c = target point defining camera sight

    # moves the position of the camera from (1,0,0) with origin in (0,0,0)
    # to (r,theta,z)=(dist*zoomceof,azimuth, elevation) with origin in c
    upvect = (0, 0, 1)
    np = c + Matrix3.axisRotation(upvect, azimuth * pi / 180) * Vector3(1, 0, 0) * camdist * zoomcoef
    np = np + Vector3(0, 0, elevation)

    zb.lookAt(np, c, upvect)

    # LIGHT: setLight(lightpos, ambiant, diffuse, specular)
    zb.setLight(lightpos, ambiant, diffuse, specular)

    # MAKE PICTURE
    zb.process(lscene)

    # SAVE IMAGE
    # avoid to save image on disk
    # imshow(z.getImage().to_array())
    iname = filename_prefix + '/' + str(suffix) + '.png'
    # print("2:image name = ", iname)
    zb.getImage().save(iname)

    return iname


# !!!!!!!!!!! NEW VERSION NOT YET TESTED --> TO BE TESTED
def take_circular_snapshots(viewer, delta_a=36, filename_prefix=".", suffix="",
                            cam_settings={'camdist': 1., 'zoomcoef': 1., 'bb': None, 'target_point': None}):
    """
    take a snapshot of a computed lpy_scene contained in the viewer.

    - delta_a is the increment angle in degrees
    """

    if 'camdist' in cam_settings:
        camdist = cam_settings['camdist']
    else:
        camdist = None
    if 'bb' in cam_settings:
        bb = cam_settings['bb']
    else:
        bb = None
    if 'zoomcoef' in cam_settings:
        zoomcoef = cam_settings['zoomcoef']
    else:
        zoomcoef = 1.0
    if 'target_point' in cam_settings:
        target_point = cam_settings['target_point']
    else:
        target_point = None
    if 'elevation' in cam_settings:
        elevation = cam_settings['elevation']
    else:
        elevation = 0.0  # elevation in screen units along the z-axis
    if 'azimuth' in cam_settings:
        azimuth = cam_settings['azimuth']
    else:
        azimuth = 0.0

    if target_point == None:
        # define bounding box used to take the picture
        if bb == None:
            # computes the bounding box of this scene
            bbx = get_bb(viewer)
        else:
            # computes the bounding box of this scene
            bbx = bb
        c = bbx.getCenter()
    else:
        c = target_point  # target point to look at is given as an argument

    # computes the  distance of the camera to the target point.
    if camdist == None:
        # p = position of the camera
        # h = head (shooting direction)
        # u = up (upward direction of the shooting)
        p, h, u = viewer.camera.getPosition()

        dist = norm(p - c)  # * camdist_factor
        # print ("scene center = ", c)
        # print ("camera position = ", p,h,u)
        # print ("camera distance = ", camdist)
    else:
        dist = camdist

    for angle in arange(0, 360, delta_a):
        # Rotate the X-direction (1,0,0) of the scene of angle degrees around the Z direction (0,0,1)
        # and assigns the initial camera dist (camdist) as the norm of the vector
        # warning: the angle in axisRotation should be given in radians.
        np = c + Matrix3.axisRotation((0, 0, 1), angle * pi / 180) * Vector3(1, 0, 0) * camdist
        np = np + Vector3(0, 0, elevation)

        # Defines the new camera position and orientation as
        # shooting direction = c-np
        # up vector of the camera keeps oriented towards Z
        viewer.camera.lookAt(np, c)
        iname = filename_prefix + '/snapshot_' + str(suffix) + '-' + str(angle) + '.png'
        viewer.saveSnapshot(iname)

    return iname


def simulate_and_shoot(model_filename, variables, suffix, cam_settings):
    """Build the L-system with its input variables

    Parameters
    ----------
    model_filename : str
        File name (and path) to the L-Py model.
    variables : ???
        ???.
    suffix : str
        The suffix string built from the list of variables. See ``build_suffix``.
    cam_settings : dict
        A dictionary of camera settings. See ``grid_simulate_and_shoot``.

    Returns
    -------
    str
        Name of the png file.

    See Also
    --------
    build_suffix
    grid_simulate_and_shoot
    take_offline_snapshot

    """
    # build the L-system with its input variables
    lsys = lpy.Lsystem(model_filename, variables)

    lstring = lsys.derive()
    lscene = lsys.sceneInterpretation(lstring)

    # uses interpretation rules to plot using the viewer
    # Note: there is no way for the moment to get the viewer activated on the lstring without plotting it

    # scans the plant iteratively starting at X = 0 and with view angles increments
    # passed in argument
    # the procedure writes on the disk the different snapshots (.png files)
    # in a file

    # create a directory to store the snapshots
    # 1. current_filename without suffix
    filename = model_filename.split('.')[0]

    # 2. build the new dir name
    newdir = 'snapshots/'  # +filename+'-'+str(suffix)
    if not os.path.exists(newdir):
        os.makedirs(newdir)

    # 3. creates a snapshot
    lpy_viewer = lsys.Viewer
    # second argument is azimuth
    # iname = take_snapshot(lpy_viewer, newdir, suffix, cam_settings = cam_settings)
    iname = take_offline_snapshot(lscene, newdir, suffix, cam_settings=cam_settings)

    # Explicitly delete this L-system
    # (not clear why needed, as this should be done from the destruction of variables local to this function, but indeed needed)
    del lsys

    return iname


def build_suffix(var, short_names=None, sep=" "):
    """Build a string from a dictionary of variables.

    Takes a dict of variables and constructs a str suffix identifying uniquely this set of parameter
    (for further defining a name corresponding to this set of parameters for storage on disk of corresponding results)

    Parameters
    ----------
    var
    short_names : ???, optional
        list of short names ???
    sep : {' ', '_', '#'}, optional
        The separator to use to concatenate the variables. Defaults to `" "`, a space.

    Returns
    -------
    str
        The string built from the dictionary of variables.

    """
    stg = ''
    index = 0
    for name in var:
        if short_names != None:
            shname = short_names[name]
        else:
            shname = str(index)
        if shname != '':  # if shorname empty, do not use this variable for suffix
            if index != 0:
                stg += sep
            val = var[name]
            if type(val) == bool:
                stg += shname + ':' + ('+' if val == True else '-')
            else:
                stg += shname + ':' + str(val)
        index += 1
    return stg


def build_variables_dict(x, y, free_variable_list, fixed_variables):
    """Builds a dictionary of variables by blending free variables (made of two variables)

    Parameters
    ----------
    x, y : float
        Values of the free variables.
    free_variable_list : list of str
        The names of the free variables `x` & `y`.
    fixed_variables : dict
        Dictionary of variables with already defined values, *i.e.* fixed.

    Returns
    -------
    dict
        Dictionary of variable names & values ``{'name': value}``.

    Raises
    ------
    ValueError
        If the `free_variable_list` is not at least of length 2.

    """
    try:
        assert len(free_variable_list) >= 2  # (at list names for x and y should be defined)
    except AssertionError:
        raise ValueError(f"Parameter `free_variable_list` should be at least of length 2, got '{len(free_variable_list)}'!")

    vardict = fixed_variables.copy()
    vardict.update({free_variable_list[0]: x})
    vardict.update({free_variable_list[1]: y})

    return vardict


def grid_simulate_and_shoot(simpoints, model_filename, free_variable_list, fixed_variables,
                            cam_settings={'camdist': 1., 'zoomcoef': 1., 'bb': None, 'target_point': None},
                            short_names=None):
    """Launch simulations of the lpy model `model_filename` for free parameters values defined in `simpoints`.

    Parameters
    ----------
    simpoints : dict
        A dictionary hose keys are the y values and the values are lists of x-values for each y key
    model_filename : str
        File name (and path) to the L-Py model.
    free_variable_list : list of str
        The names of the free variables `x` & `y`.
    fixed_variables : dict
        Dictionary of variables with already defined values, *i.e.* fixed.
    cam_settings : dict, optional
        Can be a simple dictionary like ``cam_settings = {'camdist':1., 'zoomcoef':1., 'bb':None, 'target_point':None}``.
        Or a function returning a dict for args = (x,y) = pair of values of the free parameters like
        ``cam_settings(x,y) --> {'camdist':val1, 'zoomcoef':val2, 'bb':val3, 'target_point':val4}``
    short_names : ???, optional
        list of short names ???

    """

    # create list of variable names
    fixed_variable_list = fixed_variables.keys()

    # computes the list of all time points for all md, and sorts it.
    total_simpoints = set()
    for y in simpoints:
        total_simpoints = total_simpoints.union(total_simpoints, set(simpoints[y]))
    total_simpoints = sorted(total_simpoints)
    XMAX = len(total_simpoints)  # length of the list of all the simulated timepoints merged together
    print("total_simpoints(", XMAX, ") = ", total_simpoints)

    # Now make the simulations
    YMAX = len(simpoints)
    image_name_list = []  # will contain the final image names as saved on the disk.
    coords = []  # will store for each resulting image the pairs (md,dnb) corresponding to it
    cnt = 0
    # the grid corresponds to the variation of two variables x and y (free variables)
    # in general x may represent time (and x0,x1,...,xk) a trajectory of a given value of y.
    # y represents the value of another parameter that is studied in the model with respect to the first one (x, possibly = time).
    for y in simpoints:
        for x in simpoints[y]:
            variables = build_variables_dict(x, y, free_variable_list, fixed_variables)

            suffix = build_suffix(variables, short_names)
            print('grid element:', x, y, suffix)
            if type(cam_settings) == dict:
                camdict = cam_settings  # setting is constant for every pair (x,y)
            else:  # should be a function
                assert callable(cam_settings)
                camdict = cam_settings(x, y)
            # iname = simulate_and_shoot(model_filename, variables, suffix, camdict)
            iname = simulate_and_shoot(model_filename, variables, suffix, camdict)
            image_name_list.append(iname)
            y_index = cnt
            x_index = total_simpoints.index(x)
            coords.append((y_index, x_index))
        cnt += 1
    print("image_name_list(", len(image_name_list), ") = ", image_name_list)
    print("Coords list(", len(coords), ") = ", coords)

    # Computes the "flattened" index of an image in the array (this index is used to position the image in )
    index_list = []
    for k in range(len(image_name_list)):
        xi = coords[k][0]
        yi = coords[k][1]  # index in list total_simpoints
        index_list.append(xi * XMAX + yi + 1)  # these indexes must be shifted by 1 as they start at 1
    print("array dim = ", YMAX, XMAX)
    print("index_list(", len(index_list), ") = ", index_list)

    plot_images(image_name_list, index_list, dir=".", size=(YMAX, XMAX))


def plot_images(image_name_list, index_list=None, dir='.', size=(1, 1)):
    """Plots the grid of png images @ grid positions defined by index_list.

    Parameters
    ----------
    image_name_list : list of str
        List of image files to plot.
    index_list : list, optional
        1 dimensional position, flattened 2D position in a grid of ``(size[0], size[1])``
    dir : str, optional
        specifies the directory where to print
    size : tuple, optional
        The dimensions of the underlying 2D grid

    """

    # A figure is a canvas that contains axes (= places on which to plot)
    # by default the figure has one axis
    # plot functions can be called on axes only (and not on figures)
    # but a different setting of axes can be created wth subplots

    dim2 = min(size[0] * 3, 10)
    dim1 = min(size[1] * 3, 15)
    print("Image: grid size= ", size[1], size[0], " image --> width,heigth = ", dim1, dim2)
    fig = plt.figure(figsize=(dim1, dim2))

    i = 1
    for iname in image_name_list:
        # image = mpimg.imread(dir + '/' + iname)
        image = mpimg.imread(iname)

        if not index_list == None:
            k = index_list[i - 1]
            axis = fig.add_subplot(size[0], size[1], k)
        else:
            axis = fig.add_subplot(size[0], size[1], i)
        # axis.add_artist(ab)
        axis.imshow(image)
        axis.axis('off')

        # remove stuff before last '/' and suffix .png from the name
        a = iname.split('/')
        title = a[-1]
        a = title.split('.')  # to remove ending .png
        title = a[0]  # without .png
        axis.set_title(title)

        i += 1

    # plt.grid()
    # plt.draw()

    # plt.savefig('image_array.png',bbox_inches='tight')
    # fig.suptitle("Simple geometric model simulations")
    fig.tight_layout()  # organises figure axes to minimize spacing
    plt.show()
