#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import find_packages
from setuptools import setup

description = "Create figures automatically from L-Py."
readme = open('README.md').read()

# find packages
pkgs = find_packages('src')

setup_kwds = dict(
    name='lpy_tools',
    version="0.1.0",
    description=description,
    long_description=readme,
    author="Christophe Godin, Ayan Chaudhury",
    author_email="christophe.godin@inria.fr, ayanchaudhury.cs@gmail.com",
    url='',
    license='LGPL-3.0',
    zip_safe=False,
    packages=pkgs,
    package_dir={'': 'src'},
    entry_points={},
    keywords='',
)

setup(**setup_kwds)
