# lpy_tools

This package contains algorithms that can be used with L-Py to automate particular tasks. 
Most of the tools are meant to be used with the L-Py python API in a python file.

## Getting started

### Install conda package

You can install the `openalea.lpy` & `lpy_tools` conda packages in an environment named `lpy` with python 3.9 as follows:

```shell
conda create -n lpy python=3.9
conda activate lpy
conda install openalea.lpy -c fredboudon -c conda-forge
conda install lpy_tools -c mosaic -c fredboudon -c conda-forge
```

:warning: You many need to install OpenGL system dependency. On Ubuntu `sudo apt install freeglut3-dev`.

### Install from sources

To **create a new conda environment** (will be named `lpy_tools`) and install the required dependencies:

```shell
conda create -f conda/environment.yml
```

To install dependencies **in an existing environment**, first activate it (with `conda activate <myenv>`), then:

```shell
conda env update -f conda/environment.yml
```

### Build conda package

You will need the `conda-build` package to build packages.
Install it in the `base` environment with:
```shell
conda install conda-build
```

Then you can install the library in _develop mode_ with:
```shell
python -m pip install -e .
```
> :warning: This should be done in the **activated** conda environment!


To build the `lpy_tools` conda packages, in the `base` environment from the root folder, run:

```shell
conda build -c conda-forge -c fredboudon conda/.
# Convert linux package for OSX-64:
conda convert -p osx-64 ~/miniconda3/conda-bld/linux-64/lpy_tools*  --output-dir ~/miniconda3/conda-bld/
# Convert linux package for WIN-64:
conda convert -p win-64 ~/miniconda3/conda-bld/linux-64/lpy_tools*  --output-dir ~/miniconda3/conda-bld/
```
> :warning: This should be done from the `base` environment!


You will need the `conda-verify` & `anaconda-client` package to upload packages.
Install them in the `base` environment with:
```shell
conda conda-verify anaconda-client
```

After a successful build, to upload the packages you can run:
```shell
anaconda login  # do this once
anaconda upload --user mosaic --label main ~/miniconda3/conda-bld/linux-64/lpy_tools*.tar.bz2
anaconda upload --user mosaic --label main ~/miniconda3/conda-bld/osx-64/lpy_tools*.tar.bz2
anaconda upload --user mosaic --label main ~/miniconda3/conda-bld/win-64/lpy_tools*.tar.bz2
```

Official documentation on how to upload packages [here](https://docs.anaconda.com/anaconda-repository/user-guide/tasks/pkgs/upload-pkg/).


### Conda useful commands

#### Purge built packages:
```bash
conda build purge
```

#### Clean cache & unused packages:
```bash
conda clean --all
```


## Module `snapshots`

### Explanations
It is frequently useful to produce simulation outputs where some variables are changed, 
but using the exact same camera settings, color-maps etc.

For this, the `snapshots` module contains functions that make it possible to launch L-Py from a python file and create images 
corresponding to different runs of L-Py where one or two parameters of the L-Py models are changed from the outside of the model. 
These parameters must be declared as extern(...) global variables in the L-Py model file: 
```
extern(diam = 0.3)
extern(angle = 30)
```
The function browses a list of parameter pairs defined by the user and produces sub-figures that are
all assembled into a single figure, with controlled camera settings.

### Quick example

We provide a simple example for this module in `examples/fractal_snapshots.py`.
To use it, move to the example folder and run the example with:
```shell
cd examples
ipython fractal_snapshots.py
```


## Module `point_sampler`

### Explanations

This module provides functions that make it possible to compute a point cloud corresponding to 
the point sampling of a L-Py geometric model output. Labels can be attached to each point by attaching 
a label to each individual module label using a dictionary.

This function can be called within the python shell, or as a python function.


### Quick example

We provide a simple example for this module in `examples/??.py`.
To use it, move to the example folder and run the example with:
```shell
cd examples
ipython ??.py
```
