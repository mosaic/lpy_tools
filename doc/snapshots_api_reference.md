# Reference snapshots API

## Module `lpy_tools.snapshots.lscene_snapshots`

```{eval-rst}
.. automodule:: lpy_tools.snapshots.lscene_snapshots
   :members:
```
