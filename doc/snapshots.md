# Snapshots creation

Snapshot creates figures automatically from L-Py models using the python API.

The module makes it possible to call a L-system from the user API and to plot figures corresponding to simulations obtained by varying some model parameters. 
Two parameters at most can be varied to create the figure.

The `lscene_snapshot` library offers 2 main functions:
- `simulate_and_shoot(model_filename, variables, suffix, cam_settings = camdict)`
- `grid_simulate_and_shoot(simpoints, model_filename, free_variable_list, fixed_variables_dict, cam_settings = cam_setting_fun1, short_names = variable_short_names)`

## Declaration of variables in the L-system model

The L-system must export parameters that need to be controlled from the outside of the L-system file.
Assume for instance that parameters `diam, length, age, name` and `simtime`  need to be exported.

This is done by declaring in the L-system file:
```
extern(diam = 2.3)
extern(length = 10.6)
extern(age = 20)
extern(name = 'orchid')
extern(simtime = 30)
param1 = 12.4
param2 = True
```

This means that only the variables `diam, length, age, name` and `simtime` can be changed by resetting them from the outside.
Note that `param1` and `param2` cannot be changed from the outside of the L-system file (as they are not declared as `extern`).

## Defining variables to call function `simulate_and_shoot()` of the library
A dictionary of variables chosen among the extern variable of the model system can be defined.

Example:
```python
vardict = {'diam':0.1, name:'fern', 'simtime': 38}
```
and then call the `simulate_and_shoot()` function.
```python
simulate_and_shoot(model_filename, variables, suffix, cam_settings = camdict)
```
suffix is an array of strings to store the computed images.
TODO: explain ... 

## Defining variables to call function `grid_simulate_and_shoot()` of the library

It is possible to select a pair of variables for which we want to create a figure by giving them different values.
`(x1,y1)(x2,y2), ... (xn,yn)`

For this, we first need to identify the pair of variables selected variable that we want to browse.
This is done by defining the list `free_variable_list`.
`free_variable_list` containing the list of free variables to vary for launching the set of simulations:
```python
free_variable_list = ['diam','age']
```
The first variable `diam` is considered as the x variable, while the second variable is the y variable (x will vary on the horizontal axis of the figure and y on the vertical axis).

Other extern variables of the model can be given a constant value different from that they have in the L-system file by listing them in the list `fixed_variables_dict`, and then redefining their value:
```python
fixed_variables_dict = {'length', 'name'}
```
All the other extern variables will keep constant and defined with the value given in the L-system file.

Finally, to define the list of pairs of values of the free variables x and y (here `diam` and `age`), a dictionary is defined as follows:
```
simpoints = { y1:[x1,x2,..,xk],   y2:[x1',x2',..,xl'], y3: ... ,   yn:[x1'',x2'', ..,xl''] }
```
`simpoints` defines for each value of y a list of values of x for which a pair (x,y) must be computed by the simulation.
In our example _e.g._:
```python
simpoints = { 10:[1,2,3,4],   20:[1,2,3,4],   30.0:[1,2,5] }
```
Meaning that (diam,age) values `(1,10), (2,10),(3,10),(4,10),(1,20),(2,20),(3,20),...,(5,30)` will be used to launch 11 simulations.
Note that the y-arrays need not be of the same size nor have the same values. 

### Redefining variable name to be printed in figures
The dictionary `variable_short_names` can be set to give a short name to each variable (extern or intern) that will be used to the label the small images.
Only names in this list will be used to construct small image labels:
```python
variable_short_names = {'diam':'D', 'length': 'L'}
```

### Setting camera attributes

Camera attributes can be set prior to calling the simulation and shooting.
Here are the attributes that can be defined with example values:
```
target_point = None             # looks a bit above z = 0. Value is a 3-uple (x,y,z)
zoomcoef = 1.                   # increase to zoom out
camdist = 1.                    # increase to widen the observation window
elevation = 30                  # elevation of the camera sight (in degrees)
azimuth = 0.                    # Azimuth of the camera
bb = None                       # Bounding box
width = 400                     # Window size in pixels
height = 400                    # Window size in pixels
aspectratio = 1.0               # contraction of X and Y direction defined by Height/Width
znear = 0.1                     # defines the plane on which the scene is projected as the distance between camera and object that should not intersect visualized objects
zfar = 200                      # Same thing for the farthest plane in the camera. Only objects between these two planes can be seen
lightpos = (0, 0, 10)           # light characteristics 
ambiant = (255, 255, 255)
diffuse = (0, 0, 0)
specular = (0, 0, 0))
```
Then all these values can be assembled in a dictionary that will be passed to the simulation and shooting function:
```python
camdict = {'camdist':camdist, 'zoomcoef':zoomcoef, 'bb':None, 'target_point':target_point}
```
This camdict can be read directly from a LPy camera file using
```python
camdict = read_camera('my_camera.txt')
```
Alternatively, if the camera parameters depend on the values of the free variables, on can define a function of the two free variables, that will return a dictionary corresponding to each pair of free variables values:
```python
def cam_setting_fun1(x,y):
    '''
    Function that defines the camera setting values for each pair of parameter values (x,y)
    '''
    t = target_point
    z = zoomcoef
    c = camdist

    return {'camdist':c, 'zoomcoef':z, 'bb':None, 'target_point':t, 'elevation':0.0}
```
### Launching the simulation
```shell
ipython --gui=qt make_snapshots.py
```
or within `ipython`:
```shell
ipython> %gui qt        # to make sure that qt will be used for interpreting the event loop of PlantGL viewer
ipython> %run make_snapshots.py
```

### Example of use
```python
from openalea.plantgl.all import Vector3
from .lscene_snapshots import simulate_and_shoot, grid_simulate_and_shoot, plot_images, build_suffix

model_filename = 'model.lpy'
 
###########################################
# Define the simulations parameters
###########################################

free_variable_list = ['diam','age']
fixed_variables_dict = {'length', 'name'}
simpoints = { 10:[1,2,3,4],   20:[1,2,3,4],   30.0:[1,2,5] }     # {age:[list of diams], ...}

# Definition of shortnames for variables (used for generating small image names)
variable_short_names = {'diam':'D', 'length': 'L'}
 
###########################################
# Define the camera parameters
###########################################

target_point = Vector3(0,0,30.) # looks a bit above z = 0
zoomcoef = 1                    # increase to zoom out
camdist = 150.                  # increase to widen the observation window
camdict = {'camdist':camdist, 'zoomcoef':zoomcoef, 'bb':None, 'target_point':target_point}
 
grid_simulate_and_shoot(simpoints, 
                        model_filename,
                        free_variable_list, 
                        fixed_variables_dict, 
                        cam_settings = cam_dict, 
                        short_names = variable_short_names)
```