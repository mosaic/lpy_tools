'''
Author: Ayan Chaudhury (ayanchaudhury.cs@gmail.com)
Contribution: C. Godin
INRIA team MOSAIC
Updated: April 2022

Main script for point sampling (automatic generation of labelled point cloud from virtual plant models)

Usage: python generatePointCloud.py [-D filename1] [-R filename2] [-C filename3]  [-L filename4] model_filename numberOfPoints

with the correspondance for option names:
-D   --labelDictionary
-R   --rawPointCloud
-C   --coloredPointCloud
-L   --labelPointCloud

Examples:
    >> python generatePointCloud.py model_1.lpy 100                             # by default a function label map will be used for assigning ids to module labels
    >> python generatePointCloud.py model_1 10000                               # another model, with a greater number of sampled points
    >> python generatePointCloud.py -D __default__ model_2 1000                 # will use the dictionary defined by default in the generatePointCloud.py file
    >> python generatePointCloud.py -D labelDictionary.txt model_3.lpy  1000    # here, instead, will use the dictionary defined the file labelDictionary.txt
    >> python generatePointCloud.py -D labelDictionary.txt -R rawpoints.txt -C coloredpoints.txt -L labelledpoints.txt  model_4.lpy 10000

These commands may take a few minutes depending on the complexity of the model and the number of points

'''

import argparse
import ast

from openalea import lpy
from lpy_tools.point_sampler.scan_utils import pointSampler

# Default optional parameter values
labelTable = {"L": 1, "A": 2, "B": 3, "I": 4, "Flower": 5, "F": 6, "K": 7, "M": 8, "Pedicel": 7, "Petals": 8, "Carpel": 9, "Leaf": 10}
output1_filename = "coloredPointCloud.xyz"
output2_filename = "rawPointCloud.xyz"
output3_filename = "labels.txt"

# Parsing command line arguments
parser = argparse.ArgumentParser()

parser.add_argument("model_filename", type = str)
parser.add_argument("number_of_points", type = int)

parser.add_argument("-D","--labelDictionary", type = str, help="either __default__ to use the dict table provided in the python script, or a filename containing the dictionary definition ")
parser.add_argument("-R","--rawPointCloud", type = str)
parser.add_argument("-C","--coloredPointCloud", type = str)
parser.add_argument("-L","--labelPointCloud", type = str)

args = parser.parse_args()

# positional arguments
if args.model_filename:
    model_filename = args.model_filename
if args.number_of_points:
    number_of_points = args.number_of_points

LABELDICT = False
# optional arguments
if args.labelDictionary:
    LABELDICT = True
    dictFileName = args.labelDictionary
    if dictFileName != '__default__':
        # reading the label dictionary from the file
        with open(dictFileName) as f:
            data = f.read()
        # reconstructing the data as a dictionary from the python syntax
        labelTable = ast.literal_eval(data)
if args.coloredPointCloud:
    output1_filename = args.coloredPointCloud
if args.rawPointCloud:
    output2_filename = args.rawPointCloud
if args.labelPointCloud:
    output3_filename = args.labelPointCloud

# possibility to customize the label map by a user function
# that asssociates a particular integer to each lstring module using
# custom algorithm defined here.
# --> This can be tested with model_1.lpy which has this 3rd index in module 'I'

# WARNING: THIS FUNCTION HAS BEEN DESIGNED TO WORK WITH model_1.lpy
def labelMap_model_1 (lstring, id):
    # trivial example where the returned value correspond to an integer associated with the lableTable
    name = lstring[id].name
    if name == 'I':
        axis_id = lstring[id][2]
        #print('I detected: ', axis_id)
    else:
        axis_id = -1
    return axis_id

# Then run the lsystem model
lsys = lpy.Lsystem(model_filename)
lstring = lsys.derive()
lscene = lsys.sceneInterpretation(lstring)

# Finally, call the pointSampler on the lstring/lscene

if LABELDICT == True:
    # one can pass directly the labelTable.
    print("uses a label dictionary for defining module labels")
    print(labelTable)
    pointSampler(lstring, lscene, labelTable, number_of_points, output1_filename, output2_filename, output3_filename)
else:
    # Alternatively, one can call the pointSampler function with a fucntion argument to define the label map:
    print("uses a label function for defining module labels")
    labelMap = labelMap_model_1
    pointSampler(lstring, lscene, labelMap, number_of_points, output1_filename, output2_filename, output3_filename)
    # Note if labeltable is None, the default label map is used (one unique label per lstring module instance)
    #pointSampler(lstring, lscene, None, number_of_points, output1_filename, output2_filename, output3_filename)
